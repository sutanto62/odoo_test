.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

====================================
Financial Report Monthly Balance XLS
====================================

Adds XLS report to the following report with monthly balance.
 - Profit and Loss
 - Balance Sheet

Installation
============

To install this module, you need also the **report_xls** and **account_financial_report_webkit**
module located in:

https://github.com/OCA/reporting-engine

Contributors
------------

* Guanghui Chen <active@bona.com.sg>