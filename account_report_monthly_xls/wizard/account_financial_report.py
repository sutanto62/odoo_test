# -*- coding: utf-8 -*-

from openerp import models, api, fields

class AccountingReport(models.TransientModel):
    _inherit = 'accounting.report'
    _description = 'Accounting Report'

    enable_monthly = fields.Boolean('Enable Monthly', default=False)

    # def pre_print_report(self, cr, uid, ids, data, context=None):
    #     if context is None:
    #         context = {}
    #
    #     # will be used to attach the report on the main account
    #     data['ids'] = [data['form']['chart_account_id']]
    #
    #     fields_to_read = ['account_ids', 'account_level']
    #     vals = self.read(cr, uid, ids, fields_to_read, context=context)[0]
    #     data['form'].update(vals)
    #     return data
    #
    # def xls_export_monthly(self, cr, uid, ids, context=None):
    #     res = super(AccountingReport, self).check_report(cr, uid, ids, context=context)
    #     return res

    def _print_report(self, cr, uid, ids, data, context=None):
        context = context or {}
        if context.get('xls_export'):
            # we update form with display account value
            # data = self.pre_print_report(cr, uid, ids, data, context=context)
            # data = self.read(cr, uid, ids)[0]
            data['form'].update(self.read(cr, uid, ids, ['date_from_cmp',  'debit_credit', 'date_to_cmp',  'fiscalyear_id_cmp', 'period_from_cmp', 'period_to_cmp',  'filter_cmp', 'account_report_id', 'enable_filter', 'label_filter','target_move'], context=context)[0])
            return {'type': 'ir.actions.report.xml',
                    'report_name': 'account_report_monthly_xls.account_report_pl_xls',
                    'datas': data}
        else:
            return super(AccountingReport, self)._print_report(
                cr, uid, ids, data, context=context)