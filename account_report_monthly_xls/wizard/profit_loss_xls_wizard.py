# -*- coding: utf-8 -*-

from openerp import models, api

class ProfitLossXls(models.Model):
    _inherit = 'account.common.balance.report'
    _name = 'profit.loss.monthly'

    def monthly_xls_export(self, cr, uid, ids, context=None):
        return self.check_report(cr, uid, ids, context=context)

    def _print_report(self, cr, uid, ids, data, context=None):
        context = context or {}
        if context.get('xls_export'):
            # we update form with display account value
            data = self.pre_print_report(cr, uid, ids, data, context=context)
            return {'type': 'ir.actions.report.xml',
                    # 'report_name': 'account.account_report_trial_balance_xls',
                    'report_name': 'account.profit_loss_monthly_xls',
                    'datas': data}
        else:
            return super(ProfitLossXls, self)._print_report(cr, uid, ids, data, context=context)
