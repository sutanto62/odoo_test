# -*- coding: utf-8 -*-
{
    'name': "Account Report Monthly",

    'summary': """
        Monthly Balance""",

    'description': """
        This module add button to Profit Loss and Balance Sheet report with monthly balance.
    """,

    'author': "Sutanto",
    'website': "http://www.sample.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Account',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['report_xls', 'account_financial_report_webkit'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'templates.xml',
        'wizard/profit_loss_xls_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo.xml',
    ],
    'apps': True,
    'installable': True,
}