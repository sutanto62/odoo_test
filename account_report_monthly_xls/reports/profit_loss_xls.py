# -*- coding: utf-8 -*-

import xlwt
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell
from openerp.addons.account_financial_report_webkit.report.trial_balance \
    import TrialBalanceWebkit
from openerp.tools.translate import _

class ProfitLossXls(report_xls):

    def create(self, cr, uid, ids, data, context=None):
        return super(ProfitLossXls, self).create(cr, uid, ids, data, context=context)

    def generate_xls_report(self, _p, _xs, data, objects, wb):
        print 'generate xls report'
        return True

ProfitLossXls('report.account.profit_loss_monthly_xls', 'account.account')
